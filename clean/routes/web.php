<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('customer.main');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('add-staff', 'HomeController@addStaff');



//when logged in
Route::group(['middleware' =>  'customer_auth'], function (){

	Route::get('/profile','CustomerController@profile');

	Route::post('logout-customer', 'CustomerAuth\LoginController@logout');

	Route::get('request-cleaning', 'CustomerController@requestCleanUp');


Route::get('/customer-home', function(){
	return view('customer.home');


});

});

//guest

Route::group(['middleware' => 'customer_guest'], function (){

	Route::get('register-customer', 'CustomerAuth\RegisterController@showRegistrationForm');
	Route::post('register-customer', 'CustomerAuth\RegisterController@register');

	Route::get('login-customer', 'CustomerAuth\LoginController@showLoginForm');
	Route::post('login-customer', 'CustomerAuth\LoginController@login');

});



