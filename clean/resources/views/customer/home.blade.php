@extends('landing')

@section('content')
    <section id="testimonials" class="testimonial testimonial-1 bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="heading heading-3 text-center">
                        <div class="heading-bg">
                            <p class="mb-0">what people say ?</p>
                            <h2>testimonials</h2>
                        </div>
                    </div>
                    <!-- .heading end -->
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div id="testimonial-oc" class="testimonial-carousel">

                        <!-- testimonial item #1 -->
                        <div class="testimonial-item">
                            <div class="testimonial-content">
                                <div class="testimonial-img">
                                    <i class="fa fa-quote-left"></i>
                                    <img src="assets/images/testimonial/3.png" alt="author"/>
                                </div>
                                <p>The company’s experts have accumulated valuable experience in green development, being savvy to all aspects of the certification process and requirements.</p>
                            </div>
                            <div class="testimonial-divider">
                            </div>
                            <div class="testimonial-meta">
                                <strong>Begha</strong>, Art Director
                            </div>
                        </div>
                        <!-- .testimonial-item end -->

                        <!-- testimonial item #2 -->
                        <div class="testimonial-item">
                            <div class="testimonial-content">
                                <div class="testimonial-img">
                                    <i class="fa fa-quote-left"></i>
                                    <img src="assets/images/testimonial/2.png" alt="author"/>
                                </div>
                                <p>The skills of our project managers are attested by international certificates from leading overseas professional associations in the realm of construction, land use and real estate. </p>
                            </div>
                            <div class="testimonial-divider">
                            </div>
                            <div class="testimonial-meta">
                                <strong>Habaza</strong>, Civil Engineer
                            </div>
                        </div>
                        <!-- .testimonial-item end -->

                        <!-- testimonial item #3 -->
                        <div class="testimonial-item">
                            <div class="testimonial-content">
                                <div class="testimonial-img">
                                    <i class="fa fa-quote-left"></i>
                                    <img src="assets/images/testimonial/1.png" alt="author"/>
                                </div>
                                <p>Yellow Hats Group carries out full-cycle development, including the attraction of investments, construction and managing premium residential and non-residential properties.</p>
                            </div>
                            <div class="testimonial-divider">
                            </div>
                            <div class="testimonial-meta">
                                <strong>Fouad Badawy</strong>, Investor
                            </div>
                        </div>
                        <!-- .testimonial-item end -->

                        <!-- testimonial item #4 -->
                        <div class="testimonial-item">
                            <div class="testimonial-content">
                                <div class="testimonial-img">
                                    <i class="fa fa-quote-left"></i>
                                    <img src="assets/images/testimonial/3.png" alt="author"/>
                                </div>
                                <p>The skills of our project managers are attested by international certificates from leading overseas professional associations in the realm of construction, land use and real estate.</p>
                            </div>
                            <div class="testimonial-divider">
                            </div>
                            <div class="testimonial-meta">
                                <div class="testimonial-name">
                                    <strong>AYMAN FIKRY</strong>, Civil Engineer
                                </div>
                            </div>
                        </div>
                        <!-- .testimonial-item end -->

                        <!-- testimonial item #5 -->
                        <div class="testimonial-item">
                            <div class="testimonial-content">
                                <div class="testimonial-img">
                                    <i class="fa fa-quote-left"></i>
                                    <img src="assets/images/testimonial/1.png" alt="author"/>
                                </div>
                                <p>Yellow Hats Group carries out full-cycle development, including the attraction of investments, construction and managing premium residential and non-residential properties.</p>
                            </div>
                            <div class="testimonial-divider">
                            </div>
                            <div class="testimonial-meta">
                                <strong>Fouad Badawy</strong>, Investor
                            </div>
                        </div>
                        <!-- .testimonial-item end -->

                        <!-- testimonial item #6 -->
                        <div class="testimonial-item">
                            <div class="testimonial-content">
                                <div class="testimonial-img">
                                    <i class="fa fa-quote-left"></i>
                                    <img src="assets/images/testimonial/2.png" alt="author"/>
                                </div>
                                <p>The skills of our project managers are attested by international certificates from leading overseas professional associations in the realm of construction, land use and real estate. </p>
                            </div>
                            <div class="testimonial-divider">
                            </div>
                            <div class="testimonial-meta">
                                <strong>Habaza</strong>, Civil Engineer
                            </div>
                        </div>
                        <!-- .testimonial-item end -->

                    </div>
                </div>
                <!-- .col-md-12 end -->

            </div>
            <!-- .row end -->

        </div>
        <!-- .container end -->

    </section>
@endsection