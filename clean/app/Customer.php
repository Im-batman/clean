<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
	use Notifiable;


    //
	protected $table = 'customer';
	protected $primaryKey = 'cid';

	protected  $guard ='customer';

//
//	protected $fillable = [
//		'name', 'email', 'password',
//	];

protected $guarded = [ ];



	//hidden attributes
	protected $hidden = [
		'password', 'remember_token',
	];




//
}
