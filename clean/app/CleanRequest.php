<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CleanRequest extends Model
{
    //

	protected  $primaryKey ='rid';

	protected  $table = 'request';
}
