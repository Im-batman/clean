<?php

namespace App\Http\Middleware;

use Closure;
use function Faker\Provider\pt_BR\check_digit;
use Illuminate\Support\Facades\Auth;

class RedirectIfCustomerAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

    	if (Auth::guard()->check()){
    		return redirect('/home');
    }

    if (Auth::guard('customer')->check()){
    		return redirect('customer-home');
    }



        return $next($request);
    }
}

