<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//		 $clean_request = Request::where('status','waiting' )->get();
//		 $errands = Request::where('status', 'eaiting')->get();


        return view('dash'
//	        ,[
//        	'cleans' =>  $clean_request,
//	        'arrands' => $errands
//        ]
        )
	        ;

    }

	public function assignCleaner( Request $request, $rid  ) {
		$cleaner = $request->input('uid')->get();
    	$clean = Request::where( 'rid', $rid)->get();
    	$clean->cid = $cleaner;
    	$clean->status = 'ongoing';
    }


}
